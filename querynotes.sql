INSERT INTO albums ()


INSERT INTO albums (album_name, year, artist_id) VALUES ("Psy 6", "2012-1-1",(SELECT id FROM artists WHERE name ="Psy"));

INSERT INTO albums (album_name, year, artist_id) VALUES ("Trip", "1996-1-1",(SELECT id FROM artists WHERE name ="Rivermaya"));


-- for multiple values at once
INSERT INTO albums (album_name, year, artist_id) VALUES ("Psy 6", "2012-1-1",(SELECT id FROM artists WHERE name ="Psy"),("Trip", "1996-1-1",(SELECT id FROM artists WHERE name ="Rivermaya"));


INSERT INTO songs (title,length,genre,album_id) VALUES 
("Gangnam Style","253","K-pop",(SELECT id FROM albums WHERE album_name ="Psy 6")),
("Kundiman","234","OPM",(SELECT id FROM albums WHERE album_name ="Trip")),
("Kisapmata","319","OPM",(SELECT id FROM albums WHERE album_name ="Trip"));


DELETE FROM table1 WHERE condition



INSERT INTO artists (name) VALUES 
("Taylor Swift"),
("Lady Gaga"),
("Justin Bieber"),
("Ariana Grande"),
("Bruno Mars"),;